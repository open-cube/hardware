EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 5900 2100 1    50   Input ~ 0
3V3
Text HLabel 4600 5400 0    50   Input ~ 0
S2_UART_IN
Text HLabel 6500 2750 2    50   Input ~ 0
S2_UART_OUT
Wire Wire Line
	4800 4350 4450 4350
Wire Wire Line
	4800 4250 4800 4350
Wire Wire Line
	4450 4900 4800 4900
Wire Wire Line
	4450 4650 4450 4900
Connection ~ 4450 4900
Wire Wire Line
	4250 4900 4450 4900
Wire Wire Line
	4250 4650 4250 4900
Wire Wire Line
	4450 3950 4450 4050
Connection ~ 4250 3200
Wire Wire Line
	4250 4050 4250 3200
$Comp
L Device:R R?
U 1 1 629162E8
P 4450 4200
AR Path="/629162E8" Ref="R?"  Part="1" 
AR Path="/61E01EAF/629162E8" Ref="R?"  Part="1" 
AR Path="/62E3015C/629162E8" Ref="R81"  Part="1" 
F 0 "R81" H 4520 4246 50  0000 L CNN
F 1 "4K7" H 4520 4155 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 4380 4200 50  0001 C CNN
F 3 "~" H 4450 4200 50  0001 C CNN
	1    4450 4200
	1    0    0    -1  
$EndComp
Connection ~ 4450 4350
$Comp
L Device:C C?
U 1 1 629162EF
P 4450 4500
AR Path="/629162EF" Ref="C?"  Part="1" 
AR Path="/61E01EAF/629162EF" Ref="C?"  Part="1" 
AR Path="/62E3015C/629162EF" Ref="C122"  Part="1" 
F 0 "C122" H 4565 4546 50  0000 L CNN
F 1 "10p" H 4565 4455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4488 4350 50  0001 C CNN
F 3 "~" H 4450 4500 50  0001 C CNN
	1    4450 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Schottky_x2_Serial_ACK D?
U 1 1 629162F5
P 4250 4350
AR Path="/629162F5" Ref="D?"  Part="1" 
AR Path="/61E01EAF/629162F5" Ref="D?"  Part="1" 
AR Path="/62E3015C/629162F5" Ref="D14"  Part="1" 
F 0 "D14" V 4296 4429 50  0000 L CNN
F 1 "bav199" H 4000 4600 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4250 4350 50  0001 C CNN
F 3 "~" H 4250 4350 50  0001 C CNN
	1    4250 4350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3050 3850 3100 3850
Text HLabel 3050 3850 0    50   Input ~ 0
S2_AN_OUT
Wire Wire Line
	3400 3400 3400 3200
Wire Wire Line
	2650 3400 3400 3400
Wire Wire Line
	2650 4150 2650 3400
Wire Wire Line
	3150 4150 2650 4150
Connection ~ 4250 4900
Wire Wire Line
	3800 4900 4250 4900
Wire Wire Line
	3800 4150 3800 4900
Wire Wire Line
	3400 3200 3800 3200
Wire Wire Line
	3800 3200 4250 3200
Connection ~ 3800 3200
Wire Wire Line
	3800 3550 3800 3200
Wire Wire Line
	6350 3200 6350 4150
Connection ~ 3400 3200
Wire Wire Line
	3250 3200 3400 3200
Text HLabel 3250 3200 0    50   Input ~ 0
5V
Wire Wire Line
	6100 4050 6100 4900
Connection ~ 3800 4900
Wire Wire Line
	3250 4900 3800 4900
Text HLabel 3250 4900 0    50   Input ~ 0
GND
Wire Wire Line
	3450 3850 3400 3850
Wire Wire Line
	3450 3950 3450 3850
Wire Wire Line
	3750 4150 3800 4150
$Comp
L Device:R R?
U 1 1 62916313
P 3250 3850
AR Path="/62916313" Ref="R?"  Part="1" 
AR Path="/61E01EAF/62916313" Ref="R?"  Part="1" 
AR Path="/62E3015C/62916313" Ref="R78"  Part="1" 
F 0 "R78" V 3457 3850 50  0000 C CNN
F 1 "1K" V 3366 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 3180 3850 50  0001 C CNN
F 3 "~" H 3250 3850 50  0001 C CNN
	1    3250 3850
	0    -1   -1   0   
$EndComp
$Comp
L Device:D_Schottky_x2_Serial_ACK D?
U 1 1 62916319
P 3450 4150
AR Path="/62916319" Ref="D?"  Part="1" 
AR Path="/61E01EAF/62916319" Ref="D?"  Part="1" 
AR Path="/62E3015C/62916319" Ref="D13"  Part="1" 
F 0 "D13" V 3496 4229 50  0000 L CNN
F 1 "bav199" H 3300 4400 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3450 4150 50  0001 C CNN
F 3 "~" H 3450 4150 50  0001 C CNN
	1    3450 4150
	-1   0    0    1   
$EndComp
Connection ~ 3800 4150
$Comp
L Device:C C?
U 1 1 62916320
P 3800 4000
AR Path="/62916320" Ref="C?"  Part="1" 
AR Path="/61E01EAF/62916320" Ref="C?"  Part="1" 
AR Path="/62E3015C/62916320" Ref="C121"  Part="1" 
F 0 "C121" H 3915 4046 50  0000 L CNN
F 1 "330p" H 3915 3955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3838 3850 50  0001 C CNN
F 3 "~" H 3800 4000 50  0001 C CNN
	1    3800 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 3850 3800 3850
Connection ~ 3800 3850
Wire Wire Line
	3800 3850 6650 3850
Connection ~ 3450 3850
$Comp
L Device:R R?
U 1 1 6291632A
P 3600 3850
AR Path="/6291632A" Ref="R?"  Part="1" 
AR Path="/61E01EAF/6291632A" Ref="R?"  Part="1" 
AR Path="/62E3015C/6291632A" Ref="R79"  Part="1" 
F 0 "R79" V 3807 3850 50  0000 C CNN
F 1 "1K" V 3716 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 3530 3850 50  0001 C CNN
F 3 "~" H 3600 3850 50  0001 C CNN
	1    3600 3850
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 62916330
P 3800 3700
AR Path="/62916330" Ref="R?"  Part="1" 
AR Path="/61E01EAF/62916330" Ref="R?"  Part="1" 
AR Path="/62E3015C/62916330" Ref="R80"  Part="1" 
F 0 "R80" H 3870 3746 50  0000 L CNN
F 1 "10K" H 3870 3655 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 3730 3700 50  0001 C CNN
F 3 "~" H 3800 3700 50  0001 C CNN
	1    3800 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 62916336
P 5800 5400
AR Path="/62916336" Ref="R?"  Part="1" 
AR Path="/61E01EAF/62916336" Ref="R?"  Part="1" 
AR Path="/62E3015C/62916336" Ref="R83"  Part="1" 
F 0 "R83" V 5593 5400 50  0000 C CNN
F 1 "470R" V 5900 5400 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 5730 5400 50  0001 C CNN
F 3 "~" H 5800 5400 50  0001 C CNN
	1    5800 5400
	0    1    1    0   
$EndComp
Wire Wire Line
	6650 4150 6350 4150
Wire Wire Line
	6650 4050 6100 4050
$Comp
L Connector:6P6C J?
U 1 1 6291633E
P 7050 4050
AR Path="/6291633E" Ref="J?"  Part="1" 
AR Path="/61E01EAF/6291633E" Ref="J?"  Part="1" 
AR Path="/62E3015C/6291633E" Ref="J12"  Part="1" 
F 0 "J12" H 6720 4054 50  0000 R CNN
F 1 "SENSOR_2" H 6720 4145 50  0000 R CNN
F 2 "Mylibs:RJ12_DS1133-S60BP" V 7050 4075 50  0001 C CNN
F 3 "~" V 7050 4075 50  0001 C CNN
	1    7050 4050
	-1   0    0    1   
$EndComp
$Comp
L 74xGxx:SN74LVC1G14DBV U?
U 1 1 62916344
P 5900 2750
AR Path="/61D904B4/62916344" Ref="U?"  Part="1" 
AR Path="/61E01EAF/62916344" Ref="U?"  Part="1" 
AR Path="/62E3015C/62916344" Ref="U36"  Part="1" 
F 0 "U36" H 5656 2796 50  0000 R CNN
F 1 "74HC2G14" H 5656 2705 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 5900 2500 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74lvc1g14.pdf" H 5900 2750 50  0001 C CNN
	1    5900 2750
	1    0    0    1   
$EndComp
Wire Wire Line
	6500 2750 6200 2750
Wire Wire Line
	5700 2750 5200 2750
Wire Wire Line
	5200 2750 5200 3950
Wire Wire Line
	4450 3950 5200 3950
Wire Wire Line
	5900 2100 5900 2250
Wire Wire Line
	5900 2950 5900 3000
Connection ~ 5900 4900
Wire Wire Line
	5900 4900 6100 4900
$Comp
L Device:C C?
U 1 1 62916352
P 6050 2250
AR Path="/62916352" Ref="C?"  Part="1" 
AR Path="/61E01EAF/62916352" Ref="C?"  Part="1" 
AR Path="/62E3015C/62916352" Ref="C124"  Part="1" 
F 0 "C124" H 6165 2296 50  0000 L CNN
F 1 "100n" H 6165 2205 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6088 2100 50  0001 C CNN
F 3 "~" H 6050 2250 50  0001 C CNN
	1    6050 2250
	0    1    1    0   
$EndComp
Connection ~ 5900 2250
Wire Wire Line
	5900 2250 5900 2550
Wire Wire Line
	6200 2250 6250 2250
Wire Wire Line
	6250 2250 6250 3000
Wire Wire Line
	6250 3000 5900 3000
Connection ~ 5900 3000
Wire Wire Line
	5900 3000 5900 4900
$Comp
L 74xGxx:SN74LVC1G14DBV U?
U 1 1 6291635F
P 5350 5400
AR Path="/61D904B4/6291635F" Ref="U?"  Part="1" 
AR Path="/61E01EAF/6291635F" Ref="U?"  Part="1" 
AR Path="/62E3015C/6291635F" Ref="U35"  Part="1" 
F 0 "U35" H 5106 5446 50  0000 R CNN
F 1 "74HC2G14" H 5106 5355 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 5350 5150 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74lvc1g14.pdf" H 5350 5400 50  0001 C CNN
	1    5350 5400
	1    0    0    1   
$EndComp
Wire Wire Line
	4600 5400 5150 5400
Wire Wire Line
	5350 5200 5350 5000
Connection ~ 5350 3200
Wire Wire Line
	5350 3200 5650 3200
Wire Wire Line
	5350 5600 6100 5600
Wire Wire Line
	6100 5600 6100 4900
Connection ~ 6100 4900
Wire Wire Line
	5950 5400 6250 5400
Wire Wire Line
	6250 5400 6250 4600
$Comp
L Device:C C?
U 1 1 6291636E
P 5500 5000
AR Path="/6291636E" Ref="C?"  Part="1" 
AR Path="/61E01EAF/6291636E" Ref="C?"  Part="1" 
AR Path="/62E3015C/6291636E" Ref="C123"  Part="1" 
F 0 "C123" H 5615 5046 50  0000 L CNN
F 1 "100n" H 5615 4955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5538 4850 50  0001 C CNN
F 3 "~" H 5500 5000 50  0001 C CNN
	1    5500 5000
	0    1    1    0   
$EndComp
Connection ~ 5350 5000
Wire Wire Line
	5350 5000 5350 3200
Wire Wire Line
	5650 5000 5650 4900
Connection ~ 5650 4900
Wire Wire Line
	5650 4900 5900 4900
$Comp
L Device:D_Schottky_x2_Serial_ACK D?
U 1 1 62916379
P 5650 4600
AR Path="/62916379" Ref="D?"  Part="1" 
AR Path="/61E01EAF/62916379" Ref="D?"  Part="1" 
AR Path="/62E3015C/62916379" Ref="D15"  Part="1" 
F 0 "D15" V 5696 4679 50  0000 L CNN
F 1 "bav199" H 5400 4850 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5650 4600 50  0001 C CNN
F 3 "~" H 5650 4600 50  0001 C CNN
	1    5650 4600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5850 4600 6250 4600
Wire Wire Line
	5650 4300 5650 3200
Wire Wire Line
	6650 4350 5800 4350
Wire Wire Line
	5800 4350 5800 4250
Wire Wire Line
	5800 4250 4800 4250
Wire Wire Line
	6250 4600 6250 4250
Wire Wire Line
	6250 4250 6650 4250
Connection ~ 6250 4600
$Comp
L Device:R R?
U 1 1 6251400F
P 4800 4650
AR Path="/6251400F" Ref="R?"  Part="1" 
AR Path="/61E01EAF/6251400F" Ref="R?"  Part="1" 
AR Path="/62E3015C/6251400F" Ref="R82"  Part="1" 
F 0 "R82" H 4870 4696 50  0000 L CNN
F 1 "100K" H 4870 4605 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 4730 4650 50  0001 C CNN
F 3 "~" H 4800 4650 50  0001 C CNN
	1    4800 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 4500 4800 4350
Connection ~ 4800 4350
Wire Wire Line
	4800 4800 4800 4900
Connection ~ 4800 4900
Wire Wire Line
	4800 4900 5650 4900
Wire Wire Line
	4250 3200 5350 3200
Text HLabel 6250 3200 0    50   Input ~ 0
5Vp
Wire Wire Line
	6250 3200 6350 3200
$EndSCHEMATC
