EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L byDave:BD63573NUV U?
U 1 1 61DED16F
P 5050 3450
AR Path="/61DED16F" Ref="U?"  Part="1" 
AR Path="/61D86EDF/61DED16F" Ref="U?"  Part="1" 
AR Path="/61D903CF/61DED16F" Ref="U23"  Part="1" 
F 0 "U23" H 5300 2950 50  0000 L CNN
F 1 "BD63573NUV" H 4350 2950 50  0000 L CNN
F 2 "Package_SON:VSON-10-1EP_3x3mm_P0.5mm_EP1.65x2.4mm_ThermalVias" H 4950 3450 50  0001 C CNN
F 3 "" H 4950 3450 50  0001 C CNN
	1    5050 3450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 3950 5050 3950
Connection ~ 5150 3950
Connection ~ 5050 3950
Wire Wire Line
	5050 3950 5150 3950
Text HLabel 5100 1500 1    50   Input ~ 0
VM
Text HLabel 5000 2400 1    50   Input ~ 0
+3V3
Wire Wire Line
	5000 2400 5000 2700
Text HLabel 2850 5050 0    50   Input ~ 0
GND
Wire Wire Line
	6600 3450 6600 4100
Wire Wire Line
	5150 3950 5150 4100
Connection ~ 5150 4100
Wire Wire Line
	5150 4100 5700 4100
Text HLabel 2850 4450 0    50   Input ~ 0
MOTOR3_ENCO_A
Text HLabel 2850 4850 0    50   Input ~ 0
MOTOR3_ENCO_B
Text HLabel 3400 3500 0    50   Input ~ 0
MOTOR3_PWM
Text HLabel 3400 3400 0    50   Input ~ 0
MOTOR3_DIR
$Comp
L Device:R R?
U 1 1 61DED192
P 5450 4450
AR Path="/61D86EDF/61DED192" Ref="R?"  Part="1" 
AR Path="/61D903CF/61DED192" Ref="R45"  Part="1" 
F 0 "R45" V 5243 4450 50  0000 C CNN
F 1 "4K7" V 5334 4450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 5380 4450 50  0001 C CNN
F 3 "~" H 5450 4450 50  0001 C CNN
	1    5450 4450
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 61DED198
P 5250 4600
AR Path="/61D86EDF/61DED198" Ref="C?"  Part="1" 
AR Path="/61D903CF/61DED198" Ref="C78"  Part="1" 
F 0 "C78" H 5365 4646 50  0000 L CNN
F 1 "100p" H 5365 4555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5288 4450 50  0001 C CNN
F 3 "~" H 5250 4600 50  0001 C CNN
	1    5250 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 61DED1A4
P 5800 4900
AR Path="/61D86EDF/61DED1A4" Ref="C?"  Part="1" 
AR Path="/61D903CF/61DED1A4" Ref="C79"  Part="1" 
F 0 "C79" H 5915 4946 50  0000 L CNN
F 1 "100p" H 5915 4855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5838 4750 50  0001 C CNN
F 3 "~" H 5800 4900 50  0001 C CNN
	1    5800 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 4100 5150 4100
Wire Wire Line
	5800 4750 6400 4750
Wire Wire Line
	5300 4450 5250 4450
Connection ~ 5250 4450
Wire Wire Line
	5250 4450 4350 4450
Wire Wire Line
	5800 4750 5400 4750
Wire Wire Line
	5400 4750 5400 4850
Connection ~ 5800 4750
$Comp
L 74xGxx:SN74LVC1G14DBV U?
U 1 1 61DED1B7
P 4150 4450
AR Path="/61D86EDF/61DED1B7" Ref="U?"  Part="1" 
AR Path="/61D903CF/61DED1B7" Ref="U22"  Part="1" 
F 0 "U22" H 3906 4496 50  0000 R CNN
F 1 "74HC2G14" H 3906 4405 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4150 4200 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74lvc1g14.pdf" H 4150 4450 50  0001 C CNN
	1    4150 4450
	-1   0    0    -1  
$EndComp
$Comp
L 74xGxx:SN74LVC1G14DBV U?
U 1 1 61DED1BD
P 3800 4850
AR Path="/61D86EDF/61DED1BD" Ref="U?"  Part="1" 
AR Path="/61D903CF/61DED1BD" Ref="U21"  Part="1" 
F 0 "U21" H 3556 4896 50  0000 R CNN
F 1 "74HC2G14" H 3556 4805 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 3800 4600 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74lvc1g14.pdf" H 3800 4850 50  0001 C CNN
	1    3800 4850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4000 4850 5400 4850
Wire Wire Line
	4800 5050 4800 4100
Connection ~ 4800 5050
Connection ~ 5250 5050
Wire Wire Line
	5250 4750 5250 5050
Wire Wire Line
	5800 5050 5250 5050
Wire Wire Line
	5250 5050 4800 5050
Wire Wire Line
	3800 5050 4150 5050
Wire Wire Line
	4150 4650 4150 5050
Connection ~ 4150 5050
Wire Wire Line
	4150 5050 4800 5050
Wire Wire Line
	2850 4450 3850 4450
Wire Wire Line
	3500 4850 2850 4850
$Comp
L Device:C C?
U 1 1 61DED1D0
P 3950 4100
AR Path="/61D86EDF/61DED1D0" Ref="C?"  Part="1" 
AR Path="/61D903CF/61DED1D0" Ref="C72"  Part="1" 
F 0 "C72" V 3800 3750 50  0000 C CNN
F 1 "100n" V 3900 3750 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3988 3950 50  0001 C CNN
F 3 "~" H 3950 4100 50  0001 C CNN
	1    3950 4100
	0    1    1    0   
$EndComp
$Comp
L Device:C C?
U 1 1 61DED1D6
P 3950 3850
AR Path="/61D86EDF/61DED1D6" Ref="C?"  Part="1" 
AR Path="/61D903CF/61DED1D6" Ref="C71"  Part="1" 
F 0 "C71" V 3750 3500 50  0000 C CNN
F 1 "100n" V 3850 3500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3988 3700 50  0001 C CNN
F 3 "~" H 3950 3850 50  0001 C CNN
	1    3950 3850
	0    1    1    0   
$EndComp
Wire Wire Line
	4150 4250 3800 4250
Wire Wire Line
	3800 4250 3800 4100
Connection ~ 3800 4250
Wire Wire Line
	3800 4650 3800 4250
Wire Wire Line
	3800 2700 3800 3600
Connection ~ 5000 2700
Wire Wire Line
	5000 2700 5000 2950
Connection ~ 3800 3850
Wire Wire Line
	2850 5050 3800 5050
Connection ~ 3800 5050
Connection ~ 3800 4100
Wire Wire Line
	4100 3850 4100 4100
Wire Wire Line
	4100 4100 4300 4100
Connection ~ 4100 4100
Connection ~ 4800 4100
$Comp
L Device:C C?
U 1 1 61DED1EB
P 4000 2850
AR Path="/61D86EDF/61DED1EB" Ref="C?"  Part="1" 
AR Path="/61D903CF/61DED1EB" Ref="C73"  Part="1" 
F 0 "C73" V 3950 2500 50  0000 C CNN
F 1 "100n" V 4050 2500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4038 2700 50  0001 C CNN
F 3 "~" H 4000 2850 50  0001 C CNN
	1    4000 2850
	-1   0    0    1   
$EndComp
Connection ~ 4000 2700
Wire Wire Line
	4000 2700 3800 2700
$Comp
L Device:C C?
U 1 1 61DED1F3
P 4250 2850
AR Path="/61D86EDF/61DED1F3" Ref="C?"  Part="1" 
AR Path="/61D903CF/61DED1F3" Ref="C74"  Part="1" 
F 0 "C74" V 3998 2850 50  0000 C CNN
F 1 "10u" V 4089 2850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4288 2700 50  0001 C CNN
F 3 "~" H 4250 2850 50  0001 C CNN
	1    4250 2850
	-1   0    0    1   
$EndComp
Connection ~ 4250 2700
Wire Wire Line
	4250 2700 4000 2700
$Comp
L Device:C C?
U 1 1 61DED1FB
P 5250 2500
AR Path="/61D86EDF/61DED1FB" Ref="C?"  Part="1" 
AR Path="/61D903CF/61DED1FB" Ref="C76"  Part="1" 
F 0 "C76" V 5200 2200 50  0000 C CNN
F 1 "10u" V 5300 2200 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5288 2350 50  0001 C CNN
F 3 "~" H 5250 2500 50  0001 C CNN
	1    5250 2500
	0    -1   -1   0   
$EndComp
Connection ~ 5100 2500
Wire Wire Line
	5100 2500 5100 2700
$Comp
L Device:C C?
U 1 1 61DED203
P 5250 2700
AR Path="/61D86EDF/61DED203" Ref="C?"  Part="1" 
AR Path="/61D903CF/61DED203" Ref="C77"  Part="1" 
F 0 "C77" V 4998 2700 50  0000 C CNN
F 1 "100n" V 5089 2700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5288 2550 50  0001 C CNN
F 3 "~" H 5250 2700 50  0001 C CNN
	1    5250 2700
	0    -1   -1   0   
$EndComp
Connection ~ 5100 2700
Wire Wire Line
	5100 2700 5100 2950
Wire Wire Line
	5400 2500 5400 2700
Wire Wire Line
	5400 2700 5700 2700
Wire Wire Line
	5700 2700 5700 4100
Connection ~ 5400 2700
Connection ~ 5700 4100
Wire Wire Line
	4250 3000 4100 3000
Wire Wire Line
	4100 3000 4100 3850
Connection ~ 4100 3850
Wire Wire Line
	4000 3000 4100 3000
Connection ~ 4100 3000
Text HLabel 6600 2500 0    50   Input ~ 0
+5V
$Comp
L Device:Thermistor_PTC TH?
U 1 1 61DED219
P 5100 1800
AR Path="/61D86EDF/61DED219" Ref="TH?"  Part="1" 
AR Path="/61D903CF/61DED219" Ref="TH3"  Part="1" 
F 0 "TH3" H 5197 1846 50  0000 L CNN
F 1 "1.5A" H 5197 1755 50  0000 L CNN
F 2 "Fuse:Fuse_1210_3225Metric" H 5150 1600 50  0001 L CNN
F 3 "~" H 5100 1800 50  0001 C CNN
	1    5100 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 1650 5100 1500
Wire Wire Line
	3400 3500 4550 3500
Wire Wire Line
	4250 2700 5000 2700
Wire Wire Line
	4550 3600 3800 3600
Connection ~ 3800 3600
Wire Wire Line
	3800 3600 3800 3850
Wire Wire Line
	3400 3400 4550 3400
Text HLabel 3400 3300 0    50   Input ~ 0
MOTOR3_ENABLE
$Comp
L Device:R R?
U 1 1 61DED232
P 4300 3800
AR Path="/61D86EDF/61DED232" Ref="R?"  Part="1" 
AR Path="/61D903CF/61DED232" Ref="R44"  Part="1" 
F 0 "R44" V 4093 3800 50  0000 C CNN
F 1 "10K" V 4184 3800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 4230 3800 50  0001 C CNN
F 3 "~" H 4300 3800 50  0001 C CNN
	1    4300 3800
	-1   0    0    1   
$EndComp
Wire Wire Line
	3800 3850 3800 4100
Wire Wire Line
	3400 3300 4300 3300
Wire Wire Line
	4300 3650 4300 3300
Connection ~ 4300 3300
Wire Wire Line
	4300 3300 4550 3300
Wire Wire Line
	4300 3950 4300 4100
Connection ~ 4300 4100
Wire Wire Line
	4300 4100 4800 4100
Wire Wire Line
	6750 2500 6750 3550
Wire Wire Line
	6600 2500 6750 2500
Wire Wire Line
	6800 3750 7100 3750
Wire Wire Line
	6800 4750 6800 3750
Wire Wire Line
	6700 4750 6800 4750
Wire Wire Line
	6700 4450 5600 4450
Wire Wire Line
	6700 3650 6700 4450
$Comp
L Device:R R?
U 1 1 61DED19E
P 6550 4750
AR Path="/61D86EDF/61DED19E" Ref="R?"  Part="1" 
AR Path="/61D903CF/61DED19E" Ref="R46"  Part="1" 
F 0 "R46" V 6343 4750 50  0000 C CNN
F 1 "4K7" V 6434 4750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 6480 4750 50  0001 C CNN
F 3 "~" H 6550 4750 50  0001 C CNN
	1    6550 4750
	0    1    1    0   
$EndComp
Wire Wire Line
	6700 3650 7100 3650
Wire Wire Line
	7100 3550 6750 3550
Wire Wire Line
	7100 3450 6600 3450
$Comp
L Connector:6P6C J?
U 1 1 61DC1A0A
P 7500 3450
AR Path="/61DC1A0A" Ref="J?"  Part="1" 
AR Path="/61D86EDF/61DC1A0A" Ref="J?"  Part="1" 
AR Path="/61D903CF/61DC1A0A" Ref="J9"  Part="1" 
F 0 "J9" H 7170 3454 50  0000 R CNN
F 1 "MOTOR3" H 7170 3545 50  0000 R CNN
F 2 "Mylibs:RJ12_DS1133-S60BP" V 7500 3475 50  0001 C CNN
F 3 "~" V 7500 3475 50  0001 C CNN
	1    7500 3450
	-1   0    0    1   
$EndComp
Wire Wire Line
	5100 1950 5100 2500
Wire Wire Line
	5550 3250 7100 3250
Wire Wire Line
	7100 3350 5950 3350
Wire Wire Line
	5950 3350 5950 3650
Wire Wire Line
	5550 3650 5950 3650
Wire Wire Line
	5700 4100 6600 4100
$EndSCHEMATC
