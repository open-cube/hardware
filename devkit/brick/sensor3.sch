EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 5150 1750 1    50   Input ~ 0
3V3
Text HLabel 3850 5200 0    50   Input ~ 0
S3_UART_IN
Text HLabel 5750 2350 2    50   Input ~ 0
S3_UART_OUT
Wire Wire Line
	4050 3950 3700 3950
Wire Wire Line
	4050 3850 4050 3950
Wire Wire Line
	3700 4500 4050 4500
Wire Wire Line
	3700 4250 3700 4500
Connection ~ 3700 4500
Wire Wire Line
	3500 4500 3700 4500
Wire Wire Line
	3500 4250 3500 4500
Wire Wire Line
	3700 2350 3700 3650
Connection ~ 3500 2800
Wire Wire Line
	3500 3650 3500 2800
$Comp
L Device:R R?
U 1 1 62533754
P 3700 3800
AR Path="/62533754" Ref="R?"  Part="1" 
AR Path="/61E01EAF/62533754" Ref="R?"  Part="1" 
AR Path="/62E3015C/62533754" Ref="R?"  Part="1" 
AR Path="/62EAE0DC/62533754" Ref="R87"  Part="1" 
F 0 "R87" H 3770 3846 50  0000 L CNN
F 1 "4K7" H 3770 3755 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 3630 3800 50  0001 C CNN
F 3 "~" H 3700 3800 50  0001 C CNN
	1    3700 3800
	1    0    0    -1  
$EndComp
Connection ~ 3700 3950
$Comp
L Device:C C?
U 1 1 6253375B
P 3700 4100
AR Path="/6253375B" Ref="C?"  Part="1" 
AR Path="/61E01EAF/6253375B" Ref="C?"  Part="1" 
AR Path="/62E3015C/6253375B" Ref="C?"  Part="1" 
AR Path="/62EAE0DC/6253375B" Ref="C126"  Part="1" 
F 0 "C126" H 3815 4146 50  0000 L CNN
F 1 "10p" H 3815 4055 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3738 3950 50  0001 C CNN
F 3 "~" H 3700 4100 50  0001 C CNN
	1    3700 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Schottky_x2_Serial_ACK D?
U 1 1 62533761
P 3500 3950
AR Path="/62533761" Ref="D?"  Part="1" 
AR Path="/61E01EAF/62533761" Ref="D?"  Part="1" 
AR Path="/62E3015C/62533761" Ref="D?"  Part="1" 
AR Path="/62EAE0DC/62533761" Ref="D17"  Part="1" 
F 0 "D17" V 3546 4029 50  0000 L CNN
F 1 "bav199" H 3250 4200 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3500 3950 50  0001 C CNN
F 3 "~" H 3500 3950 50  0001 C CNN
	1    3500 3950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2300 3450 2350 3450
Text HLabel 2300 3450 0    50   Input ~ 0
S3_AN_OUT
Wire Wire Line
	2650 3000 2650 2800
Wire Wire Line
	1900 3000 2650 3000
Wire Wire Line
	1900 3750 1900 3000
Wire Wire Line
	2400 3750 1900 3750
Connection ~ 3500 4500
Wire Wire Line
	3050 4500 3500 4500
Wire Wire Line
	3050 3750 3050 4500
Wire Wire Line
	2650 2800 3050 2800
Wire Wire Line
	3050 2800 3500 2800
Connection ~ 3050 2800
Wire Wire Line
	3050 3150 3050 2800
Wire Wire Line
	5700 2800 5700 3750
Connection ~ 2650 2800
Wire Wire Line
	2500 2800 2650 2800
Text HLabel 2500 2800 0    50   Input ~ 0
5V
Wire Wire Line
	5350 3650 5350 4500
Connection ~ 3050 4500
Wire Wire Line
	2500 4500 3050 4500
Text HLabel 2500 4500 0    50   Input ~ 0
GND
Wire Wire Line
	2700 3450 2650 3450
Wire Wire Line
	2700 3550 2700 3450
Wire Wire Line
	3000 3750 3050 3750
$Comp
L Device:R R?
U 1 1 6253377F
P 2500 3450
AR Path="/6253377F" Ref="R?"  Part="1" 
AR Path="/61E01EAF/6253377F" Ref="R?"  Part="1" 
AR Path="/62E3015C/6253377F" Ref="R?"  Part="1" 
AR Path="/62EAE0DC/6253377F" Ref="R84"  Part="1" 
F 0 "R84" V 2707 3450 50  0000 C CNN
F 1 "1K" V 2616 3450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 2430 3450 50  0001 C CNN
F 3 "~" H 2500 3450 50  0001 C CNN
	1    2500 3450
	0    -1   -1   0   
$EndComp
$Comp
L Device:D_Schottky_x2_Serial_ACK D?
U 1 1 62533785
P 2700 3750
AR Path="/62533785" Ref="D?"  Part="1" 
AR Path="/61E01EAF/62533785" Ref="D?"  Part="1" 
AR Path="/62E3015C/62533785" Ref="D?"  Part="1" 
AR Path="/62EAE0DC/62533785" Ref="D16"  Part="1" 
F 0 "D16" V 2746 3829 50  0000 L CNN
F 1 "bav199" H 2550 4000 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 2700 3750 50  0001 C CNN
F 3 "~" H 2700 3750 50  0001 C CNN
	1    2700 3750
	-1   0    0    1   
$EndComp
Connection ~ 3050 3750
$Comp
L Device:C C?
U 1 1 6253378C
P 3050 3600
AR Path="/6253378C" Ref="C?"  Part="1" 
AR Path="/61E01EAF/6253378C" Ref="C?"  Part="1" 
AR Path="/62E3015C/6253378C" Ref="C?"  Part="1" 
AR Path="/62EAE0DC/6253378C" Ref="C125"  Part="1" 
F 0 "C125" H 3165 3646 50  0000 L CNN
F 1 "330p" H 3165 3555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3088 3450 50  0001 C CNN
F 3 "~" H 3050 3600 50  0001 C CNN
	1    3050 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 3450 3050 3450
Connection ~ 3050 3450
Wire Wire Line
	3050 3450 5900 3450
Connection ~ 2700 3450
$Comp
L Device:R R?
U 1 1 62533796
P 2850 3450
AR Path="/62533796" Ref="R?"  Part="1" 
AR Path="/61E01EAF/62533796" Ref="R?"  Part="1" 
AR Path="/62E3015C/62533796" Ref="R?"  Part="1" 
AR Path="/62EAE0DC/62533796" Ref="R85"  Part="1" 
F 0 "R85" V 3057 3450 50  0000 C CNN
F 1 "1K" V 2966 3450 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 2780 3450 50  0001 C CNN
F 3 "~" H 2850 3450 50  0001 C CNN
	1    2850 3450
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 6253379C
P 3050 3300
AR Path="/6253379C" Ref="R?"  Part="1" 
AR Path="/61E01EAF/6253379C" Ref="R?"  Part="1" 
AR Path="/62E3015C/6253379C" Ref="R?"  Part="1" 
AR Path="/62EAE0DC/6253379C" Ref="R86"  Part="1" 
F 0 "R86" H 3120 3346 50  0000 L CNN
F 1 "10K" H 3120 3255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 2980 3300 50  0001 C CNN
F 3 "~" H 3050 3300 50  0001 C CNN
	1    3050 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 625337A2
P 5050 5200
AR Path="/625337A2" Ref="R?"  Part="1" 
AR Path="/61E01EAF/625337A2" Ref="R?"  Part="1" 
AR Path="/62E3015C/625337A2" Ref="R?"  Part="1" 
AR Path="/62EAE0DC/625337A2" Ref="R89"  Part="1" 
F 0 "R89" V 4843 5200 50  0000 C CNN
F 1 "470R" V 5150 5200 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 4980 5200 50  0001 C CNN
F 3 "~" H 5050 5200 50  0001 C CNN
	1    5050 5200
	0    1    1    0   
$EndComp
Wire Wire Line
	5900 3750 5700 3750
Wire Wire Line
	5900 3650 5350 3650
$Comp
L Connector:6P6C J?
U 1 1 625337AA
P 6300 3650
AR Path="/625337AA" Ref="J?"  Part="1" 
AR Path="/61E01EAF/625337AA" Ref="J?"  Part="1" 
AR Path="/62E3015C/625337AA" Ref="J?"  Part="1" 
AR Path="/62EAE0DC/625337AA" Ref="J13"  Part="1" 
F 0 "J13" H 5970 3654 50  0000 R CNN
F 1 "SENSOR_3" H 5970 3745 50  0000 R CNN
F 2 "Mylibs:RJ12_DS1133-S60BP" V 6300 3675 50  0001 C CNN
F 3 "~" V 6300 3675 50  0001 C CNN
	1    6300 3650
	-1   0    0    1   
$EndComp
$Comp
L 74xGxx:SN74LVC1G14DBV U?
U 1 1 625337B0
P 5150 2350
AR Path="/61D904B4/625337B0" Ref="U?"  Part="1" 
AR Path="/61E01EAF/625337B0" Ref="U?"  Part="1" 
AR Path="/62E3015C/625337B0" Ref="U?"  Part="1" 
AR Path="/62EAE0DC/625337B0" Ref="U38"  Part="1" 
F 0 "U38" H 4906 2396 50  0000 R CNN
F 1 "74HC2G14" H 4906 2305 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 5150 2100 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74lvc1g14.pdf" H 5150 2350 50  0001 C CNN
	1    5150 2350
	1    0    0    1   
$EndComp
Wire Wire Line
	5750 2350 5450 2350
Wire Wire Line
	5150 1750 5150 1900
Wire Wire Line
	5150 2550 5150 2600
Connection ~ 5150 4500
Wire Wire Line
	5150 4500 5350 4500
$Comp
L Device:C C?
U 1 1 625337BE
P 5300 1900
AR Path="/625337BE" Ref="C?"  Part="1" 
AR Path="/61E01EAF/625337BE" Ref="C?"  Part="1" 
AR Path="/62E3015C/625337BE" Ref="C?"  Part="1" 
AR Path="/62EAE0DC/625337BE" Ref="C128"  Part="1" 
F 0 "C128" H 5415 1946 50  0000 L CNN
F 1 "100n" H 5415 1855 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5338 1750 50  0001 C CNN
F 3 "~" H 5300 1900 50  0001 C CNN
	1    5300 1900
	0    1    1    0   
$EndComp
Connection ~ 5150 1900
Wire Wire Line
	5150 1900 5150 2150
Wire Wire Line
	5450 1900 5500 1900
Wire Wire Line
	5500 1900 5500 2600
Wire Wire Line
	5500 2600 5150 2600
Connection ~ 5150 2600
Wire Wire Line
	5150 2600 5150 4500
$Comp
L 74xGxx:SN74LVC1G14DBV U?
U 1 1 625337CB
P 4600 5200
AR Path="/61D904B4/625337CB" Ref="U?"  Part="1" 
AR Path="/61E01EAF/625337CB" Ref="U?"  Part="1" 
AR Path="/62E3015C/625337CB" Ref="U?"  Part="1" 
AR Path="/62EAE0DC/625337CB" Ref="U37"  Part="1" 
F 0 "U37" H 4356 5246 50  0000 R CNN
F 1 "74HC2G14" H 4356 5155 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 4600 4950 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74lvc1g14.pdf" H 4600 5200 50  0001 C CNN
	1    4600 5200
	1    0    0    1   
$EndComp
Wire Wire Line
	3850 5200 4400 5200
Wire Wire Line
	4600 5000 4600 4800
Connection ~ 4600 2800
Wire Wire Line
	4600 2800 4900 2800
Wire Wire Line
	4600 5400 5350 5400
Wire Wire Line
	5350 5400 5350 4500
Connection ~ 5350 4500
Wire Wire Line
	5200 5200 5500 5200
Wire Wire Line
	5500 5200 5500 4200
$Comp
L Device:C C?
U 1 1 625337DA
P 4750 4800
AR Path="/625337DA" Ref="C?"  Part="1" 
AR Path="/61E01EAF/625337DA" Ref="C?"  Part="1" 
AR Path="/62E3015C/625337DA" Ref="C?"  Part="1" 
AR Path="/62EAE0DC/625337DA" Ref="C127"  Part="1" 
F 0 "C127" H 4865 4846 50  0000 L CNN
F 1 "100n" H 4865 4755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4788 4650 50  0001 C CNN
F 3 "~" H 4750 4800 50  0001 C CNN
	1    4750 4800
	0    1    1    0   
$EndComp
Connection ~ 4600 4800
Wire Wire Line
	4600 4800 4600 2800
Wire Wire Line
	4900 4800 4900 4500
Connection ~ 4900 4500
Wire Wire Line
	4900 4500 5150 4500
$Comp
L Device:D_Schottky_x2_Serial_ACK D?
U 1 1 625337E5
P 4900 4200
AR Path="/625337E5" Ref="D?"  Part="1" 
AR Path="/61E01EAF/625337E5" Ref="D?"  Part="1" 
AR Path="/62E3015C/625337E5" Ref="D?"  Part="1" 
AR Path="/62EAE0DC/625337E5" Ref="D18"  Part="1" 
F 0 "D18" V 4946 4279 50  0000 L CNN
F 1 "bav199" H 4650 4450 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4900 4200 50  0001 C CNN
F 3 "~" H 4900 4200 50  0001 C CNN
	1    4900 4200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5100 4200 5500 4200
Wire Wire Line
	4900 3900 4900 2800
Wire Wire Line
	5900 3950 5050 3950
Wire Wire Line
	5050 3950 5050 3850
Wire Wire Line
	5050 3850 4050 3850
Wire Wire Line
	5500 4200 5500 3850
Wire Wire Line
	5500 3850 5900 3850
Connection ~ 5500 4200
$Comp
L Device:R R?
U 1 1 625337F5
P 4050 4250
AR Path="/625337F5" Ref="R?"  Part="1" 
AR Path="/61E01EAF/625337F5" Ref="R?"  Part="1" 
AR Path="/62E3015C/625337F5" Ref="R?"  Part="1" 
AR Path="/62EAE0DC/625337F5" Ref="R88"  Part="1" 
F 0 "R88" H 4120 4296 50  0000 L CNN
F 1 "100K" H 4120 4205 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 3980 4250 50  0001 C CNN
F 3 "~" H 4050 4250 50  0001 C CNN
	1    4050 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 4100 4050 3950
Connection ~ 4050 3950
Wire Wire Line
	4050 4400 4050 4500
Connection ~ 4050 4500
Wire Wire Line
	4050 4500 4900 4500
Wire Wire Line
	3500 2800 4600 2800
Text HLabel 5550 2800 0    50   Input ~ 0
5Vp
Wire Wire Line
	5550 2800 5700 2800
Wire Wire Line
	3700 2350 4950 2350
$EndSCHEMATC
