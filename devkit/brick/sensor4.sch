EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 11
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 5700 1400 1    50   Input ~ 0
3V3
Text HLabel 4400 4750 0    50   Input ~ 0
S4_UART_IN
Text HLabel 6300 2050 2    50   Input ~ 0
S4_UART_OUT
Wire Wire Line
	4600 3650 4250 3650
Wire Wire Line
	4600 3550 4600 3650
Wire Wire Line
	4250 4200 4600 4200
Wire Wire Line
	4250 3950 4250 4200
Connection ~ 4250 4200
Wire Wire Line
	4050 4200 4250 4200
Wire Wire Line
	4050 3950 4050 4200
Wire Wire Line
	4250 3250 4250 3350
Connection ~ 4050 2500
Wire Wire Line
	4050 3350 4050 2500
$Comp
L Device:R R?
U 1 1 6253DD1F
P 4250 3500
AR Path="/6253DD1F" Ref="R?"  Part="1" 
AR Path="/61E01EAF/6253DD1F" Ref="R?"  Part="1" 
AR Path="/62E3015C/6253DD1F" Ref="R?"  Part="1" 
AR Path="/62F341C8/6253DD1F" Ref="R93"  Part="1" 
F 0 "R93" H 4320 3546 50  0000 L CNN
F 1 "4K7" H 4320 3455 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 4180 3500 50  0001 C CNN
F 3 "~" H 4250 3500 50  0001 C CNN
	1    4250 3500
	1    0    0    -1  
$EndComp
Connection ~ 4250 3650
$Comp
L Device:C C?
U 1 1 6253DD26
P 4250 3800
AR Path="/6253DD26" Ref="C?"  Part="1" 
AR Path="/61E01EAF/6253DD26" Ref="C?"  Part="1" 
AR Path="/62E3015C/6253DD26" Ref="C?"  Part="1" 
AR Path="/62F341C8/6253DD26" Ref="C130"  Part="1" 
F 0 "C130" H 4365 3846 50  0000 L CNN
F 1 "10p" H 4365 3755 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4288 3650 50  0001 C CNN
F 3 "~" H 4250 3800 50  0001 C CNN
	1    4250 3800
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Schottky_x2_Serial_ACK D?
U 1 1 6253DD2C
P 4050 3650
AR Path="/6253DD2C" Ref="D?"  Part="1" 
AR Path="/61E01EAF/6253DD2C" Ref="D?"  Part="1" 
AR Path="/62E3015C/6253DD2C" Ref="D?"  Part="1" 
AR Path="/62F341C8/6253DD2C" Ref="D20"  Part="1" 
F 0 "D20" V 4096 3729 50  0000 L CNN
F 1 "bav199" H 3800 3900 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 4050 3650 50  0001 C CNN
F 3 "~" H 4050 3650 50  0001 C CNN
	1    4050 3650
	0    -1   -1   0   
$EndComp
Wire Wire Line
	2850 3150 2900 3150
Text HLabel 2850 3150 0    50   Input ~ 0
S4_AN_OUT
Wire Wire Line
	3200 2700 3200 2500
Wire Wire Line
	2450 2700 3200 2700
Wire Wire Line
	2450 3450 2450 2700
Wire Wire Line
	2950 3450 2450 3450
Connection ~ 4050 4200
Wire Wire Line
	3600 4200 4050 4200
Wire Wire Line
	3600 3450 3600 4200
Wire Wire Line
	3200 2500 3600 2500
Wire Wire Line
	3600 2500 4050 2500
Connection ~ 3600 2500
Wire Wire Line
	3600 2850 3600 2500
Wire Wire Line
	6300 2500 6300 3450
Connection ~ 3200 2500
Wire Wire Line
	3050 2500 3200 2500
Text HLabel 3050 2500 0    50   Input ~ 0
5V
Wire Wire Line
	5900 3350 5900 4200
Connection ~ 3600 4200
Wire Wire Line
	3050 4200 3600 4200
Text HLabel 3050 4200 0    50   Input ~ 0
GND
Wire Wire Line
	3250 3150 3200 3150
Wire Wire Line
	3250 3250 3250 3150
Wire Wire Line
	3550 3450 3600 3450
$Comp
L Device:R R?
U 1 1 6253DD4A
P 3050 3150
AR Path="/6253DD4A" Ref="R?"  Part="1" 
AR Path="/61E01EAF/6253DD4A" Ref="R?"  Part="1" 
AR Path="/62E3015C/6253DD4A" Ref="R?"  Part="1" 
AR Path="/62F341C8/6253DD4A" Ref="R90"  Part="1" 
F 0 "R90" V 3257 3150 50  0000 C CNN
F 1 "1K" V 3166 3150 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 2980 3150 50  0001 C CNN
F 3 "~" H 3050 3150 50  0001 C CNN
	1    3050 3150
	0    -1   -1   0   
$EndComp
$Comp
L Device:D_Schottky_x2_Serial_ACK D?
U 1 1 6253DD50
P 3250 3450
AR Path="/6253DD50" Ref="D?"  Part="1" 
AR Path="/61E01EAF/6253DD50" Ref="D?"  Part="1" 
AR Path="/62E3015C/6253DD50" Ref="D?"  Part="1" 
AR Path="/62F341C8/6253DD50" Ref="D19"  Part="1" 
F 0 "D19" V 3296 3529 50  0000 L CNN
F 1 "bav199" H 3100 3700 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 3250 3450 50  0001 C CNN
F 3 "~" H 3250 3450 50  0001 C CNN
	1    3250 3450
	-1   0    0    1   
$EndComp
Connection ~ 3600 3450
$Comp
L Device:C C?
U 1 1 6253DD57
P 3600 3300
AR Path="/6253DD57" Ref="C?"  Part="1" 
AR Path="/61E01EAF/6253DD57" Ref="C?"  Part="1" 
AR Path="/62E3015C/6253DD57" Ref="C?"  Part="1" 
AR Path="/62F341C8/6253DD57" Ref="C129"  Part="1" 
F 0 "C129" H 3715 3346 50  0000 L CNN
F 1 "330p" H 3715 3255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3638 3150 50  0001 C CNN
F 3 "~" H 3600 3300 50  0001 C CNN
	1    3600 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 3150 3600 3150
Connection ~ 3600 3150
Wire Wire Line
	3600 3150 6450 3150
Connection ~ 3250 3150
$Comp
L Device:R R?
U 1 1 6253DD61
P 3400 3150
AR Path="/6253DD61" Ref="R?"  Part="1" 
AR Path="/61E01EAF/6253DD61" Ref="R?"  Part="1" 
AR Path="/62E3015C/6253DD61" Ref="R?"  Part="1" 
AR Path="/62F341C8/6253DD61" Ref="R91"  Part="1" 
F 0 "R91" V 3607 3150 50  0000 C CNN
F 1 "1K" V 3516 3150 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 3330 3150 50  0001 C CNN
F 3 "~" H 3400 3150 50  0001 C CNN
	1    3400 3150
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 6253DD67
P 3600 3000
AR Path="/6253DD67" Ref="R?"  Part="1" 
AR Path="/61E01EAF/6253DD67" Ref="R?"  Part="1" 
AR Path="/62E3015C/6253DD67" Ref="R?"  Part="1" 
AR Path="/62F341C8/6253DD67" Ref="R92"  Part="1" 
F 0 "R92" H 3670 3046 50  0000 L CNN
F 1 "10K" H 3670 2955 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 3530 3000 50  0001 C CNN
F 3 "~" H 3600 3000 50  0001 C CNN
	1    3600 3000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 6253DD6D
P 5600 4750
AR Path="/6253DD6D" Ref="R?"  Part="1" 
AR Path="/61E01EAF/6253DD6D" Ref="R?"  Part="1" 
AR Path="/62E3015C/6253DD6D" Ref="R?"  Part="1" 
AR Path="/62F341C8/6253DD6D" Ref="R95"  Part="1" 
F 0 "R95" V 5393 4750 50  0000 C CNN
F 1 "470R" V 5700 4750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 5530 4750 50  0001 C CNN
F 3 "~" H 5600 4750 50  0001 C CNN
	1    5600 4750
	0    1    1    0   
$EndComp
Wire Wire Line
	6450 3450 6300 3450
Wire Wire Line
	6450 3350 5900 3350
$Comp
L Connector:6P6C J?
U 1 1 6253DD75
P 6850 3350
AR Path="/6253DD75" Ref="J?"  Part="1" 
AR Path="/61E01EAF/6253DD75" Ref="J?"  Part="1" 
AR Path="/62E3015C/6253DD75" Ref="J?"  Part="1" 
AR Path="/62F341C8/6253DD75" Ref="J14"  Part="1" 
F 0 "J14" H 6520 3354 50  0000 R CNN
F 1 "SENSOR_4" H 6520 3445 50  0000 R CNN
F 2 "Mylibs:RJ12_DS1133-S60BP" V 6850 3375 50  0001 C CNN
F 3 "~" V 6850 3375 50  0001 C CNN
	1    6850 3350
	-1   0    0    1   
$EndComp
$Comp
L 74xGxx:SN74LVC1G14DBV U?
U 1 1 6253DD7B
P 5700 2050
AR Path="/61D904B4/6253DD7B" Ref="U?"  Part="1" 
AR Path="/61E01EAF/6253DD7B" Ref="U?"  Part="1" 
AR Path="/62E3015C/6253DD7B" Ref="U?"  Part="1" 
AR Path="/62F341C8/6253DD7B" Ref="U40"  Part="1" 
F 0 "U40" H 5456 2096 50  0000 R CNN
F 1 "74HC2G14" H 5456 2005 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 5700 1800 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74lvc1g14.pdf" H 5700 2050 50  0001 C CNN
	1    5700 2050
	1    0    0    1   
$EndComp
Wire Wire Line
	6300 2050 6000 2050
Wire Wire Line
	5500 2050 5000 2050
Wire Wire Line
	5000 2050 5000 3250
Wire Wire Line
	4250 3250 5000 3250
Wire Wire Line
	5700 1400 5700 1550
Wire Wire Line
	5700 2250 5700 2300
Connection ~ 5700 4200
Wire Wire Line
	5700 4200 5900 4200
$Comp
L Device:C C?
U 1 1 6253DD89
P 5850 1550
AR Path="/6253DD89" Ref="C?"  Part="1" 
AR Path="/61E01EAF/6253DD89" Ref="C?"  Part="1" 
AR Path="/62E3015C/6253DD89" Ref="C?"  Part="1" 
AR Path="/62F341C8/6253DD89" Ref="C132"  Part="1" 
F 0 "C132" H 5965 1596 50  0000 L CNN
F 1 "100n" H 5965 1505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5888 1400 50  0001 C CNN
F 3 "~" H 5850 1550 50  0001 C CNN
	1    5850 1550
	0    1    1    0   
$EndComp
Connection ~ 5700 1550
Wire Wire Line
	5700 1550 5700 1850
Wire Wire Line
	6000 1550 6050 1550
Wire Wire Line
	6050 1550 6050 2300
Wire Wire Line
	6050 2300 5700 2300
Connection ~ 5700 2300
Wire Wire Line
	5700 2300 5700 4200
$Comp
L 74xGxx:SN74LVC1G14DBV U?
U 1 1 6253DD96
P 5150 4750
AR Path="/61D904B4/6253DD96" Ref="U?"  Part="1" 
AR Path="/61E01EAF/6253DD96" Ref="U?"  Part="1" 
AR Path="/62E3015C/6253DD96" Ref="U?"  Part="1" 
AR Path="/62F341C8/6253DD96" Ref="U39"  Part="1" 
F 0 "U39" H 4906 4796 50  0000 R CNN
F 1 "74HC2G14" H 4906 4705 50  0000 R CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 5150 4500 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/sn74lvc1g14.pdf" H 5150 4750 50  0001 C CNN
	1    5150 4750
	1    0    0    1   
$EndComp
Wire Wire Line
	4400 4750 4950 4750
Wire Wire Line
	5150 4550 5150 4300
Connection ~ 5150 2500
Wire Wire Line
	5150 2500 5450 2500
Wire Wire Line
	5150 4950 5900 4950
Wire Wire Line
	5900 4950 5900 4200
Connection ~ 5900 4200
Wire Wire Line
	5750 4750 6050 4750
Wire Wire Line
	6050 4750 6050 3900
$Comp
L Device:C C?
U 1 1 6253DDA5
P 5300 4300
AR Path="/6253DDA5" Ref="C?"  Part="1" 
AR Path="/61E01EAF/6253DDA5" Ref="C?"  Part="1" 
AR Path="/62E3015C/6253DDA5" Ref="C?"  Part="1" 
AR Path="/62F341C8/6253DDA5" Ref="C131"  Part="1" 
F 0 "C131" H 5415 4346 50  0000 L CNN
F 1 "100n" H 5415 4255 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5338 4150 50  0001 C CNN
F 3 "~" H 5300 4300 50  0001 C CNN
	1    5300 4300
	0    1    1    0   
$EndComp
Connection ~ 5150 4300
Wire Wire Line
	5150 4300 5150 2500
Wire Wire Line
	5450 4300 5450 4200
Connection ~ 5450 4200
Wire Wire Line
	5450 4200 5700 4200
$Comp
L Device:D_Schottky_x2_Serial_ACK D?
U 1 1 6253DDB0
P 5450 3900
AR Path="/6253DDB0" Ref="D?"  Part="1" 
AR Path="/61E01EAF/6253DDB0" Ref="D?"  Part="1" 
AR Path="/62E3015C/6253DDB0" Ref="D?"  Part="1" 
AR Path="/62F341C8/6253DDB0" Ref="D21"  Part="1" 
F 0 "D21" V 5496 3979 50  0000 L CNN
F 1 "bav199" H 5200 4150 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 5450 3900 50  0001 C CNN
F 3 "~" H 5450 3900 50  0001 C CNN
	1    5450 3900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5650 3900 6050 3900
Wire Wire Line
	5450 3600 5450 2500
Wire Wire Line
	6450 3650 5600 3650
Wire Wire Line
	5600 3650 5600 3550
Wire Wire Line
	5600 3550 4600 3550
Wire Wire Line
	6050 3900 6050 3550
Wire Wire Line
	6050 3550 6450 3550
Connection ~ 6050 3900
$Comp
L Device:R R?
U 1 1 6253DDC0
P 4600 3950
AR Path="/6253DDC0" Ref="R?"  Part="1" 
AR Path="/61E01EAF/6253DDC0" Ref="R?"  Part="1" 
AR Path="/62E3015C/6253DDC0" Ref="R?"  Part="1" 
AR Path="/62F341C8/6253DDC0" Ref="R94"  Part="1" 
F 0 "R94" H 4670 3996 50  0000 L CNN
F 1 "100K" H 4670 3905 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 4530 3950 50  0001 C CNN
F 3 "~" H 4600 3950 50  0001 C CNN
	1    4600 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 3800 4600 3650
Connection ~ 4600 3650
Wire Wire Line
	4600 4100 4600 4200
Connection ~ 4600 4200
Wire Wire Line
	4600 4200 5450 4200
Wire Wire Line
	4050 2500 5150 2500
Text HLabel 6000 2500 0    50   Input ~ 0
5Vp
Wire Wire Line
	6000 2500 6300 2500
$EndSCHEMATC
