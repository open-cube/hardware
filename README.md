# Hardware kostky Open-Cube

Co to je Open-Cube? Z [https://maglab.fel.cvut.cz/open-cube/](https://maglab.fel.cvut.cz/open-cube/):

> Alternativní otevřená řídicí kostka pro komponenty z LEGO setů NXT a EV3 (senzory a motory). Založená na Raspberry Pi Pico (ARM mikrokontrolér RP2040), programovatelná v jazyce MicroPython.

Tento repozitář obsahuje schéma zapojení a návrh PCB pro současnou verzi Open-Cube. Návrh byl vytvořen pomocí KiCad 6.0.
 * Ve složce [devkit/brick](devkit/brick) ([PDF](devkit/brick/LEGOkostka.pdf)) naleznete schéma hlavní řídící desky.
 * Ve složce [devkit/buttons](devkit/buttons) ([PDF](devkit/brick/LEGOtlacitka.pdf)) naleznete schéma pomocné desky s tlačítky a LCD displejem.

## Blokový diagram

![Konceptuální přehled zapojení Open-Cube.](images/open-cube_editDN.drawio.png)

## Obrázky

Sestavená kostka při pohledu zepředu:
![Sestavená kostka](images/cube_in_box_side.jpg)

Kostka po otevření. Vespod je vidět hlavní PCB, na ní je připevněná
deska s tlačítky a patice pro dvě 18650 baterie.
![Kostka po otevření](images/cube_in_box.jpg)

Hlavní PCB zespoda:
![Hlavní PCB zespoda](images/cube_dps1.jpg)

Porovnání NXT a Open-Cube:
![Porovnání NXT a Open-Cube](images/cube_NXT_compare.jpg)


## Errata

Schémata v tomto repozitáři odpovídají vyrobeným PCB.
Od té doby jsme v nich nalezli pár chybiček:

 * Původní návrh počítal s použitím invertujících Schmitt triggerů 74HC2G14
   na UART pinech senzorových vstupů. Jako vhodnější se ale ukázalo
   použití bufferů, které pak byly osazeny na PCB.
 * Na současné PCB není možné hardwarově vypnout napájecí zdroj kostky
   jen pomocí tlačítka ON/OFF. Místo toho je tlačítko propojkou přivedeno na
   pin P10 extenderu PCF8575 a vypínání se provádí softwarově.
 * Na PCB jsou špatně zapojené piny UART1_RX/UART1_TX a UART5_TX/UART5_RX.
   Zabudovaná HW UART periferie v RP2040 totiž potřebuje obrácenou orientaci RX/TX pinů.
   Některým kostkám jsme proto RX/TX prohodili pomocí přídavných propojek.

## Autoři

Elektroniku navrhl ing. David Novotný z Katedry měření FEL ČVUT.

## License

TODO
